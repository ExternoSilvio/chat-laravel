<?php

namespace App\Http\Controllers;

use App\Events\MessageSentEvent;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function admin()
    {
        return view('adminchat');
    }

    public function getUsers() {
        return User::all();
    }

    public function getMessagesByUser(Request $request, $id) {
        $request->user()->authorizeRoles(['admin', 'superadmin']);
        return Message::where('user_id', $id)->orWhere('to_id', $id)->with('user')->get();
    }

    // get messages
    public function fetch()
    {
        // validating user roles
        $request->user()->authorizeRoles(['admin', 'superadmin']);
        return Message::with('user')->get();
    }

    // send admin message
    public function sendAdminMessage(Request $request) {
        // validating user roles
        $request->user()->authorizeRoles(['admin', 'superadmin']);

        // dd($request);

        $user = Auth::user();
        $message = Message::create([
            'message' => $request->message,
            'to_id' => $request->toId,
            'user_id' => Auth::user()->id
        ]);
        
        // live message
        broadcast(new MessageSentEvent($user, $message))->toOthers();
    }

    // send user message
    public function sentMessage(Request $request)
    {
        $user = Auth::user();
        $message = Message::create([
            'message' => $request->message,
            'user_id' => Auth::user()->id,
            'to_id' => null
        ]);

        // live message
        broadcast(new MessageSentEvent($user, $message))->toOthers();
    }
}
