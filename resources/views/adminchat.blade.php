@extends('layouts.app')

@section('content')
    <admin-chat :user="{{ Auth::user() }}"></admin-chat>
@endsection