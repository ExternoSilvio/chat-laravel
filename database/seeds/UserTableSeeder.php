<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_superadmin = Role::where('role', 'superadmin')->first();
        $role_admin = Role::where('role', 'admin')->first();

        $user = new User();
        $user->name = 'silvio';
        $user->email = 'silviowow.155@gmail.com';
        $user->password = bcrypt('silvio1234');
        $user->save();
        $user->roles()->attach($role_superadmin);

        $user = new User();
        $user->name = 'celmy';
        $user->email = 'celmymima@gmail.com';
        $user->password = bcrypt('celmy1234');
        $user->save();
    }
}
